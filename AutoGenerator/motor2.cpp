#include "motor2.h"

Motor2::Motor2()
{

}

std::string Motor2::parseSideDir(SideDir sideDir_arg)
{
    switch (sideDir_arg)
    {
    case LEFT:
        return "LEFT";
        break;
    case RIGHT:
        return "RIGHT";
        break;
    default:
        return "NULL";
        break;
    }
}

SideDir Motor2::parseString(std::string temp)
{
   if (temp.compare("LEFT") == 0)
   {
       return LEFT;
   }
   else if (temp.compare("RIGHT") == 0)
   {
       return RIGHT;
   }
   else
   {
       return Null;
   }
}


Motor2::Motor2(std::string motNameArg, bool invertArg, bool driveMotArg)
{
    inverted = invertArg;
    motorName = motNameArg;
    driveMotor = driveMotArg;
    sideDir = Null;
}
Motor2::Motor2(std::string motNameArg, bool invertArg, bool driveMotArg, SideDir sideDir_arg)
{
    inverted = invertArg;
    motorName = motNameArg;
    driveMotor = driveMotArg;
    sideDir = sideDir_arg;
}

bool Motor2::getInversion()
{
    return inverted;
}

bool Motor2::isDriveMotor()
{
    return driveMotor;
}

std::string Motor2::getMotorName()
{
    return motorName;
}

void Motor2::setDriveMotor(bool driveMotArg)
{
    driveMotor = driveMotArg;
}

void Motor2::setInversion(bool invertArg)
{
    inverted = invertArg;
}

void Motor2::setMotorName(std::string motNameArg)
{
    motorName = motNameArg;
}

void Motor2::setSideDir(SideDir sidedir_arg)
{
    sideDir = sidedir_arg;
}

SideDir Motor2::getSideDir()
{
    return sideDir;
}
