#ifndef LINE_COMMAND_H
#define LINE_COMMAND_H
#include <iostream>
#include <vector>
#include <QLine>
#include <AutoGenerator/command.h>
#include <QWidget>
#include <QString>

class Line_Command
{

public:
    Line_Command();
    void set_path(std::vector<QLineF> arg_path);
    std::vector<std::string> output_commands(QString tempDir);
    void generate_commands();

private:
    std::vector<QLineF> path;
    std::vector<Command> list_commands;

};

#endif // LINE_COMMAND_H
