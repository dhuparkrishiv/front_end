#ifndef LINEXTRA_H
#define LINEXTRA_H
#include <QLine>
#include <QLineF>
#include <QPointF>

class Linextra
{
public:
    Linextra();
    Linextra(QLine line_arg);
    Linextra(QLineF line_arg);
    QPointF p1();
    QPointF p2();
    QLineF getQLineF();
    double getSlope();
private:
    double slope;
    QLineF line;
};

#endif // LINEXTRA_H
