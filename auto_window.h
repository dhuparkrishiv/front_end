#ifndef AUTO_WINDOW_H
#define AUTO_WINDOW_H

#include <QDialog>
#include <QComboBox>
#include <QString>
#include <QListWidget>
#include "AutoGenerator/generateauto.h"
#include "AutoGenerator/motorconfigw.h"


namespace Ui {
class Auto_Window;
}

enum DeviceType
{
    MotorE, Servo, Sensor
};


class Auto_Window : public QDialog
{
    Q_OBJECT

public:
    explicit Auto_Window(QWidget *parent = 0);
    ~Auto_Window();
public slots:
    virtual int exec() override;

private slots:
    void on_addDevBut_clicked();
    ///test

    void on_deviceCombo_activated(int index);

    void on_MotorConfigDestroyed();

    void on_outputCBtn_clicked();

    void on_imp_btn_clicked();

    void on_exp_config_btn_clicked();

private:
    Ui::Auto_Window *ui;
    QComboBox *deviceCombo;

        QString currentDeviceSelect;
        DeviceType currentDeviceType;
        QListWidget *deviceList;
        GenerateAuto generateAuto;
        MotorConfigW *motorConfigWin;
        void initializeDeviceCombo();
};

#endif // AUTO_WINDOW_H
